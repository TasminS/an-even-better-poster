# A modification to prevent the MPE poster template being such an eye fuck.

Step 1. Download mpecolours.sty and beamerthemempetwo.sty and put it in your directory with your poster.
Step 2. In your poster change 
{
  \usetheme{mpe}
}
to
{
  \usetheme{mpetwo}
}
Step 3. Profit.